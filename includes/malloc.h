/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/21 20:19:00 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/11 13:22:39 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of malloc project.
**   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MALLOC_MALLOC_H
# define MALLOC_MALLOC_H

# include <sys/types.h>

# ifdef __cplusplus
extern "C" {
# endif

void	free(void *ptr);
void	*malloc(size_t size) __attribute__((malloc,warn_unused_result));
void	*calloc(size_t memb_size,
				size_t amount) __attribute__((malloc,warn_unused_result));
void	*realloc(void *ptr,
				 size_t size) __attribute__((warn_unused_result));
void	show_alloc_mem(void);

# ifdef __cplusplus
}
# endif

#endif
