/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/18 15:59:00 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/11 13:55:02 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of malloc project.
**   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "ft_printf.h"
#include "malloc_internal.h"

void	show_alloc_mem(void)
{
	t_alloc_page	*page;

	LOCK_IT;
	page = g_pages;
	UNLOCK_IT;
	while (page)
	{
		print_page_info(page);
		LOCK_IT;
		page = page->next;
		UNLOCK_IT;
	}
}

void	print_page_info(const t_alloc_page *const page)
{
	t_alloc_info	*info;

	ft_printf("%s%s%s page at %p\nSize %zu bytes, still vacant: %zu bytes.\n",
				page->type == tiny ? "Tiny" : "",
				page->type == small ? "Small" : "",
				page->type == large ? "Large" : "",
				GET_USER_PG_ADDR(page), GET_USER_PG_SIZE(page), page->vacant);
	info = page->children;
	while (info)
	{
		print_alloc_info(info);
		info = info->next;
	}
	ft_putstr("\n");
}

void	print_alloc_info(const t_alloc_info *const info)
{
	char			*units;
	const size_t	size = GET_USER_AL_SIZE(info);
	size_t			amrt_size;

	units = "";
	amrt_size = size;
	if (amrt_size > 1024)
	{
		amrt_size /= 1024;
		units = "kilo";
	}
	if (amrt_size > 1024)
	{
		amrt_size /= 1024;
		units = "mega";
	}
	ft_printf("%0zx : %4zu %4sbytes", GET_USER_AL_ADDR(info), amrt_size, units);
	if (size > 1024)
	{
		ft_printf("(%zu bytes)", size);
	}
	ft_putstr(".\n");
}
