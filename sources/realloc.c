/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   realloc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/03 13:24:00 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/11 16:44:54 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of malloc project.
**   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "malloc_internal.h"

/*
**	Allocates new memory, copies the contents of the old location
**	and frees the old location.
*/

static void	*alloc_and_move(void *ptr, size_t new_size, size_t old_size)
{
	void	*result;

	if ((result = malloc(new_size)))
	{
		ft_memmove(result, ptr, MIN(old_size, new_size));
		free(ptr);
	}
	return (result);
}

/*
**	Tries to add more memory to the given address.
*/

static void	*do_realloc(void *ptr, size_t size)
{
	t_alloc_page	*page;
	t_alloc_info	*alloc;
	void			*result;

	result = NULL;
	LOCK_IT;
	alloc = find_alloc((ptrdiff_t)ptr, NULL);
	if (!alloc)
	{
		UNLOCK_IT
		return NULL;
	}
	page = alloc->page;
	if ((alloc->next && (size_t)(alloc->next->real_address -
		GET_USER_AL_ADDR(alloc)) >= size) || (!alloc->next && (size_t)
		(GET_USER_PG_ADDR(page) + GET_USER_PG_SIZE(page) -
		GET_USER_AL_ADDR(alloc) - GET_USER_AL_SIZE(alloc)) >= size))
	{
		alloc->page->vacant -= size - GET_USER_AL_SIZE(alloc);
		*((size_t*)&(alloc->real_size)) = size + sizeof(t_alloc_info);
		result = ptr;
	}
	UNLOCK_IT
	return (result ? ptr : alloc_and_move(ptr, size, GET_USER_AL_SIZE(alloc)));
}

/*
**	Checks constraints and then tries to add more memory to the given address.
*/

void		*realloc(void *ptr, size_t size)
{
	if (!ptr)
		return (malloc(size));
	if (!size)
	{
		free(ptr);
		return (NULL);
	}
	return (do_realloc(ptr, size));
}
