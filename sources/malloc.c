/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/21 20:29:00 by dkovalch          #+#    #+#             */
/*   Updated: 2018/05/06 13:42:22 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of malloc project.
**   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "malloc_internal.h"

static void	*do_malloc(size_t size)
{
	t_alloc_page	*page;
	t_alloc_p_type	type;
	void			*result;

	type = size <= SMALL_SIZE ? small : large;
	type = size <= TINY_SIZE ? tiny : type;
	LOCK_IT;
	page = g_pages ? get_page(type) : add_new_page(type, size);
	result = add_new_alloc(page, size);
	UNLOCK_IT;
	return result;
}

void		*malloc(size_t size)
{
	if (size == 0)
		return (NULL);
	return (do_malloc(size));
}

void		*calloc(size_t memb_size, size_t amount)
{
	void	*result;

	if ((result = malloc(memb_size * amount)))
		ft_bzero(result, memb_size * amount);
	return (result);
}

void		free(void *ptr)
{
	if (!ptr)
		return ;
	LOCK_IT;
	remove_alloc((ptrdiff_t)ptr);
	UNLOCK_IT;
}
