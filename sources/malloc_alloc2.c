/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc_alloc2.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/03 13:55:00 by dkovalch          #+#    #+#             */
/*   Updated: 2018/05/06 14:24:27 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of malloc project.
**   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "malloc_internal.h"

/*
**	Finds alloc info corresponding to the given address.
*/

t_alloc_info		*find_alloc(ptrdiff_t user_addr, t_alloc_info **prev)
{
	t_alloc_page	*ppage;
	t_alloc_info	*result;

	result = NULL;
	ppage = g_pages;
	while (ppage && !result)
	{
		if (prev)
			*prev = NULL;
		result = ppage->children;
		while (result && (ptrdiff_t) (GET_USER_AL_ADDR(result)) != user_addr)
		{
			if (prev)
				*prev = result;
			result = result->next;
		}
		ppage = ppage->next;
	}
	if (prev && !result)
		*prev = NULL;
	return (result);
}

/*
**	Removes alloc info corresponding to the given address.
*/

void				remove_alloc(ptrdiff_t address)
{
	t_alloc_page	*page;
	t_alloc_info	*prev;
	t_alloc_info	*result;

	result = find_alloc(address, &prev);
	if (!result)
		return;
	page = result->page;
	page->vacant += result->real_size;
	if (prev)
		prev->next = result->next;
	else
		page->children = result->next;
	ft_bzero(result, sizeof(t_alloc_info));
}
