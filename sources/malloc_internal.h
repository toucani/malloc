/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc_internal.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/21 20:40:00 by dkovalch          #+#    #+#             */
/*   Updated: 2018/05/06 17:02:16 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of malloc project.
**   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MALLOC_MALLOC_INTERNAL_H
# define MALLOC_MALLOC_INTERNAL_H

# include "libft.h"
# include "malloc.h"
# include <assert.h>
# include <stddef.h>
# include <pthread.h>
# include <sys/types.h>

/*
**	These constants define sizes of corresponding pages.
**	Tweak them to improve speed/memory consumption.
*/

# define TINY_SIZE 512
# define SMALL_SIZE 2048
# define LARGE_SIZE 10240
# define MMAP_ALLOC_AMOUNT 100

/*
**	Just for convenience.
*/

# define LOCK_IT pthread_mutex_lock(&g_lock);
# define UNLOCK_IT pthread_mutex_unlock(&g_lock);

# ifdef __cplusplus
extern "C" {
# endif

/*
**	Type of the allocations memory page is supposed to have.
*/

typedef enum				e_alloc_p_type
{
	tiny,
	small,
	large
}							t_alloc_p_type;

/*
**	Holds info about the memory page allocated.
**
** 		real_address	real page address allocated(the same as &this)
**		real_size		real page size, same as user_size + sizeof(this)
** 		type			type of allocs the page is made for
**		vacant			amount of currently free bytes(real space)
*/

typedef struct				s_alloc_page
{
	const ptrdiff_t			real_address;
	const size_t			real_size;
	size_t					vacant;
	struct s_alloc_page		*next;
	struct s_alloc_info		*children;
	const t_alloc_p_type	type;
}							t_alloc_page;

/*
**	These macro reduce the structure size.
*/

# define GET_USER_PG_SIZE(page) ((page)->real_size - sizeof(t_alloc_page))
# define GET_USER_PG_ADDR(page) ((page)->real_address + sizeof(t_alloc_page))

/*
**	Holds info about the item allocated.
**
** 		real_address	real address allocated(the same as &this)
**		real_size		real item size, same as user_size + sizeof(this)
*/

typedef struct				s_alloc_info
{
	const ptrdiff_t			real_address;
	const size_t			real_size;
	struct s_alloc_page		*page;
	struct s_alloc_info		*next;
}							t_alloc_info;

/*
**	These macro reduce the structure size.
*/

# define GET_USER_AL_SIZE(alloc) ((alloc)->real_size - sizeof(t_alloc_info))
# define GET_USER_AL_ADDR(alloc) ((alloc)->real_address + sizeof(t_alloc_info))

extern t_alloc_page			*g_pages;
extern pthread_mutex_t		g_lock;

t_alloc_page				*add_new_page(t_alloc_p_type type, size_t
			size) __attribute__((malloc,warn_unused_result,returns_nonnull));
t_alloc_page				*get_page(t_alloc_p_type
									type) __attribute__((warn_unused_result));
//void						remove_page(t_alloc_page
//										*page) __attribute__((nonnull));

void						*add_new_alloc(t_alloc_page *start_page, size_t
				size) __attribute__((malloc,warn_unused_result,nonnull));
t_alloc_info				*find_alloc(ptrdiff_t user_addr,
										t_alloc_info
									**prev) __attribute__((warn_unused_result));
void						remove_alloc(ptrdiff_t address);

void						print_alloc_info(const t_alloc_info
											*info) __attribute__((nonnull));
void						print_page_info(const t_alloc_page
											*page) __attribute__((nonnull));

# ifdef __cplusplus
}
# endif

#endif
