/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_print.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/23 22:11:00 by dkovalch          #+#    #+#             */
/*   Updated: 2018/02/23 22:11:00 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of malloc project.
**   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "tests_defines.hpp"

int	main()
{
	std::cout << "Constraints\n\tTiny: " << TINY_SIZE
			  << "\n\tSmall: " << SMALL_SIZE
			  << "\n\tStandard: " << LARGE_SIZE << std::endl << std::endl,

	std::cout << "Nothing has been mallocked yet:" << std::endl;
	show_alloc_mem();

	MemArray mem(true);

	std::cout << "\nMallocking 4 bytes:" << std::endl;
	mem.Add(4);
	show_alloc_mem();

	std::cout << "\nMallocking 64 bytes:" << std::endl;
	mem.Add(64);
	show_alloc_mem();

	std::cout << "\nMallocking 512 bytes:" << std::endl;
	mem.Add(512);
	show_alloc_mem();

	std::cout << "\nMallocking kilobyte:" << std::endl;
	mem.Add(kilobyte);
	show_alloc_mem();

	std::cout << "\nMallocking 32 kilobytes:" << std::endl;
	mem.Add(32 * kilobyte);
	show_alloc_mem();

	std::cout << "\nMallocking 64 kilobytes:" << std::endl;
	mem.Add(64 * kilobyte);
	show_alloc_mem();

	std::cout << "\nFreeing everything:" << std::endl;
	mem.Clear();
	show_alloc_mem();

	std::cout << "\nMallocking megabyte:" << std::endl;
	mem.Add(megabyte);
	show_alloc_mem();

	std::cout << "\nMallocking 32 megabytes:" << std::endl;
	mem.Add(32 * megabyte);
	show_alloc_mem();

	std::cout << "\nMallocking 128 megabytes:" << std::endl;
	mem.Add(128 * megabyte);
	show_alloc_mem();

	return (0);
}
