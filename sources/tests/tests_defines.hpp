/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tests_defines.hpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/23 21:42:00 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/11 16:13:37 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of malloc project.
**   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "libft.h"
#include "malloc.h"
#include "malloc_internal.h"
#include <gtest/gtest.h>
#include <array>
#include <vector>

#define EXPECT_NULL(x) EXPECT_TRUE((x) == nullptr)
#define EXPECT_NNULL(x) EXPECT_FALSE((x) == nullptr)

const size_t kilobyte = 1024;
const size_t megabyte = kilobyte * kilobyte;

const std::array<char, 2> padding_symbols = {{'@', '#'}};

class MemArray
{
public:
	explicit MemArray(bool simulate = false) : _simulate(simulate) {};

	void Add(const size_t size, const size_t amount = 1)
	{
		for (size_t ct = 0; ct < amount; ct++)
		{
			auto * ptr = static_cast<char*>(malloc(size));
			EXPECT_NNULL(ptr);
			_mem.emplace_back(std::make_pair(ptr, size));
			if (_simulate)
			{
				//Fill the memory with the symbols
				ft_memset(_mem.back().first,
						  padding_symbols[_mem.size() % padding_symbols.size()],
						  size);
				_mem.back().first[size - 1] = '\0';

				//Check that the symbols are ok
				std::string str(_mem.back().first);
				EXPECT_EQ(str.find_first_not_of(str[0]), std::string::npos);
			}
		}
		//We should have something here.
		EXPECT_GT(_mem.size(), 0);
	};

	void Clear()
	{
		for (auto &item : _mem)
		{
			EXPECT_NNULL(item.first);
			if (_simulate)
			{
				//Check that the symbols are ok
				std::string str(item.first);
				EXPECT_EQ(str.find_first_not_of(str[0]), std::string::npos);
			}
			free(item.first);
		};
		_mem.clear();
	};

	~MemArray()
	{
		Clear();
	};

private:
	std::vector<std::pair<char*, size_t>> _mem;
	bool _simulate = false;
};
