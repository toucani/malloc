/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_calloc.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/24 22:02:00 by dkovalch          #+#    #+#             */
/*   Updated: 2018/02/24 22:02:00 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of malloc project.
**   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <vector>
#include "tests_defines.hpp"

TEST(Calloc, basic)
{
	std::vector<long*> mem;

	const auto allocIt = [&mem](const size_t size)
	{
		auto *ptr = static_cast<long*>(calloc(sizeof(long), size));
		EXPECT_NNULL(ptr);
		for (size_t ct = 0; ct < size; ct++)
			EXPECT_TRUE(ptr[ct] == 0);
		mem.push_back(ptr);
	};

	allocIt(4);
	allocIt(32);
	allocIt(64);
	allocIt(kilobyte);
	allocIt(64 * kilobyte);
	allocIt(512 * kilobyte);
	allocIt(kilobyte * kilobyte);

	for (const auto& item : mem)
		free(item);
}