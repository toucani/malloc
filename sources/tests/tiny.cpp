/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_tiny.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/23 21:38:00 by dkovalch          #+#    #+#             */
/*   Updated: 2018/02/23 21:38:00 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of malloc project.
**   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "tests_defines.hpp"

TEST(Tiny, 1bytes)
{
	MemArray mem(true);
	mem.Add(1, 4096);
}

TEST(Tiny, 64bytes)
{
	MemArray mem(true);
	mem.Add(64, 4096);
}

TEST(Tiny, 512bytes)
{
	MemArray mem(true);
	mem.Add(512, 4096);
}

TEST(Tiny, 1kb)
{
	MemArray mem(true);
	mem.Add(kilobyte, 4096);
}