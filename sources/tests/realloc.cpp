/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   realloc.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/24 22:01:00 by dkovalch          #+#    #+#             */
/*   Updated: 2018/04/11 16:40:50 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of malloc project.
**   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "tests_defines.hpp"

TEST(realloc, basic)
{
	std::vector<std::pair<size_t*, size_t>> mem;

	const auto allocIt = [&mem](const size_t amount)
	{
		for (auto a = 0; a < 5; a++)
		{
			auto *ptr = static_cast<size_t*>(malloc(sizeof(size_t) * amount));
			EXPECT_NNULL(ptr);
			for (size_t ct = 0; ct < amount; ct++)
				ptr[ct] = mem.size();
			mem.emplace_back(ptr, amount);
		}
	};

	const auto reallocIt = [&mem](const size_t newAmount)
	{
		for (size_t ct = 0; ct < mem.size(); ct++)
		{
			mem[ct].first = static_cast<size_t*>(realloc(mem[ct].first, sizeof(size_t) * newAmount));
			EXPECT_NNULL(mem[ct].first);
			for (size_t kt = 0; kt < newAmount; kt++)
			{
				if (kt < mem[ct].second)
					EXPECT_EQ(mem[ct].first[kt], ct);
				else
					//We shouldn't crash here
					mem[ct].first[kt] = ct;
			}
			mem[ct].second = newAmount;
		}
	};

    //Must not segfault.
    EXPECT_NULL(realloc(reinterpret_cast<void*>(3), 10));

	allocIt(kilobyte);
	allocIt(64 * kilobyte);
	allocIt(512 * kilobyte);
	allocIt(kilobyte * kilobyte);

	reallocIt(kilobyte * kilobyte);
	reallocIt(kilobyte);
	reallocIt(10);
	reallocIt(kilobyte * kilobyte);

	for (const auto& item : mem)
		free(item.first);
}
