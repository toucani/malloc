/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_1mb.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/23 21:38:00 by dkovalch          #+#    #+#             */
/*   Updated: 2018/02/23 21:38:00 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of malloc project.
**   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "tests_defines.hpp"

TEST(Big, 4mb)
{
	MemArray mem(true);
	mem.Add(4 * megabyte, 1);
	mem.Add(4 * megabyte, 63);
}

TEST(Big, 16mb)
{
	MemArray mem(true);
	mem.Add(16 * megabyte, 1);
	mem.Add(16 * megabyte, 31);
}

TEST(Big, 32mb)
{
	MemArray mem(true);
	mem.Add(32 * megabyte, 1);
	mem.Add(32 * megabyte, 15);
}