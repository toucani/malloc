/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   basic.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/04 17:17:00 by dkovalch          #+#    #+#             */
/*   Updated: 2018/05/06 14:59:19 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of malloc project.
**   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "tests_defines.hpp"

TEST(Basic, constraints)
{
	//Padding symbols shouldn't be the same.
	EXPECT_NE(padding_symbols[0], padding_symbols[1]);

	//We didn't malloc anything yet.
	EXPECT_NULL(g_pages);

	//We shouldn't crash here
	free(NULL);

	//The same as malloc(0)
	//EXPECT_NULL(realloc(NULL, 0));

	//The same as malloc(32)
	void *ptr = realloc(NULL, 32);
	EXPECT_NNULL(ptr);
	free(ptr);

	//Must not crash here.
	free(reinterpret_cast<void*>(1));
}

TEST(Basic, basic)
{
	char* mm = (char*)malloc(kilobyte);

	//Successful malloc
	EXPECT_NNULL(mm);

	//We shouldn't have second page yet
	EXPECT_TRUE(g_pages);

	char* mm2 = (char*)malloc(kilobyte);

	//Successful malloc
	EXPECT_TRUE(mm2);

	//It cannot be the same memory
	EXPECT_NE(mm, mm2);
	//We shouldn't have third page yet
	EXPECT_TRUE(!g_pages->next || !g_pages->next->next);

	ft_memset(mm, padding_symbols[0], kilobyte);
	ft_memset(mm2, padding_symbols[1], kilobyte);

	for (size_t ct = 0; ct < kilobyte; ct++)
	{
		//Testing if the memory overlaps. If it does, the
		//symbols initially set will be different from the
		//symbols we currently have.
		EXPECT_EQ(mm[ct], padding_symbols[0]);
		EXPECT_EQ(mm2[ct], padding_symbols[1]);
	}

	free(mm);
	free(mm2);
}
