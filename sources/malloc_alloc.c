/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc_alloc.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/28 12:09:00 by dkovalch          #+#    #+#             */
/*   Updated: 2018/03/08 20:25:39 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of malloc project.
**   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "malloc_internal.h"

t_alloc_page		*g_pages = NULL;
pthread_mutex_t		g_lock = PTHREAD_MUTEX_INITIALIZER;

/*
**	Creates new alloc entry at a given address.
*/

static t_alloc_info	*new_alloc_info(ptrdiff_t addr, size_t size)
{
	t_alloc_info	*result;

	ft_bzero((void*)addr, sizeof(t_alloc_info));
	result = (t_alloc_info*)addr;
	*((ptrdiff_t*)&(result->real_address)) = addr;
	*((ptrdiff_t*)&(result->real_size)) = size;
	return (result);
}

/*
**	Looks for the place to place to put new alloc into, returns NULL, if it
**	cannot be found.
*/

static t_alloc_info	*do_insert_alloc(t_alloc_page *page, size_t size)
{
	t_alloc_info	*curr;
	t_alloc_info	*result;

	result = NULL;
	curr = page->children;
	while (curr && curr->next && curr->next->real_address
								- curr->real_address - curr->real_size < size)
		curr = curr->next;
	if (curr && (curr->next || curr->page->real_address + curr->page->real_size
					- GET_USER_AL_ADDR(curr) - GET_USER_AL_SIZE(curr) >= size))
	{
		result = new_alloc_info(GET_USER_AL_ADDR(curr) + GET_USER_AL_SIZE(curr),
					size);
		result->next = curr->next;
		curr->next = result;
	}
	return (result);
}

/*
**	Returns the new element, or NULL if there is no room to insert it.
*/

static t_alloc_info	*insert_alloc(t_alloc_page *page, size_t size)
{
	t_alloc_info	*result;

	if (page->children == NULL)
	{
		result = new_alloc_info(GET_USER_PG_ADDR(page), size);
		page->children = result;
	}
	else
	{
		result = do_insert_alloc(page, size);
	}
	return (result);
}

/*
**	Adds new alloc info to the given page(if possible) with the given size.
**	Will create a new page, if there is no room fir the alloc.
*/

void				*add_new_alloc(t_alloc_page *start_page, size_t size)
{
	t_alloc_info	*result;

	assert(start_page);
	if (start_page->vacant >= size + sizeof(t_alloc_info))
	{
		if ((result = insert_alloc(start_page, size + sizeof(t_alloc_info))))
		{
			result->page = start_page;
			assert(result->real_size == size + sizeof(t_alloc_info));
			start_page->vacant -= result->real_size;
			return ((void *)(GET_USER_AL_ADDR(result)));
		}
	}
	if (!start_page->next)
	{
		start_page->next = add_new_page(start_page->type, size);
	}
	return (add_new_alloc(start_page->next, size));
}
