/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc_page.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/21 21:27:00 by dkovalch          #+#    #+#             */
/*   Updated: 2018/05/06 13:35:20 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of malloc project.
**   Copyright (C) 2018, Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <sys/mman.h>
#include "malloc_internal.h"

/*
**	Creates a new memory page of the given type.
*/

static t_alloc_page	*new_t_alloc_page(t_alloc_p_type type, size_t size)
{
	t_alloc_page	*result;
	void			*page_addr;
	size_t			real_size;

	real_size = (type == tiny) ? TINY_SIZE : SMALL_SIZE;
	real_size = (type == large) ? LARGE_SIZE : real_size;
	real_size = (real_size + sizeof(t_alloc_info)) *
			((type == large) ? 1 : MMAP_ALLOC_AMOUNT);
	real_size = MAX(real_size, size + sizeof(t_alloc_info))
				+ sizeof(t_alloc_page);
	if (real_size % getpagesize() != 0)
		real_size += getpagesize() - (real_size % getpagesize());
	assert(real_size % getpagesize() == 0);
	page_addr = mmap(NULL, real_size,
						PROT_READ | PROT_WRITE, MAP_ANON | MAP_PRIVATE, -1, 0);
	assert(page_addr != MAP_FAILED);
	ft_bzero(page_addr, sizeof(t_alloc_page));
	result = (t_alloc_page*)page_addr;
	*((ptrdiff_t*)(&(result->real_address))) = (ptrdiff_t)page_addr;
	*((size_t*)(&(result->real_size))) = real_size;
	*((t_alloc_p_type*)(&(result->type))) = type;
	result->vacant = GET_USER_PG_SIZE(result);
	return (result);
}

/*
**	Adds new memory page into the global list. All the pages are sorted
**	in increasing type order.
*/

t_alloc_page		*add_new_page(t_alloc_p_type type, size_t size)
{
	t_alloc_page	*curr;
	t_alloc_page	*result;

	result = new_t_alloc_page(type, size);
	curr = g_pages;
	if (!curr)
	{
		g_pages = result;
	}
	else if (curr->type > type)
	{
		g_pages = result;
		result->next = curr;
	}
	else
	{
		while (curr->next && curr->next->type <= type)
		{
			curr = curr->next;
		}
		result->next = curr->next;
		curr->next = result;
	}
	return (result);
}

/*
**	Returns first found page with given type, creates new one if
**	there are no page of the given type.
*/

t_alloc_page		*get_page(t_alloc_p_type type)
{
	t_alloc_page	*result;

	result = g_pages;
	while (result)
	{
		if (result->type == type)
			break ;
		result = result->next;
	}
	result = result ? result : add_new_page(type, 0);
	return (result);
}

/*
**	Removes a page of memory, thus effectively removing
**  all the allocs made there.
*/
/*
void				remove_page(t_alloc_page *page)
{
	t_alloc_page	*curr;

	curr = g_pages;
	if (curr == page)
	{
		g_pages = g_pages->next;
	}
	else
	{
		while (curr && curr->next != page)
			curr = curr->next;
		assert(curr);
		curr->next = page->next;
	}
	munmap((void*)(page->real_address), page->real_size);
}
*/