# malloc

This is the first after-the-internship project at 42 - a thread-safe malloc library.

The goal is to recode ```malloc, realloc``` and ```free``` functions from libc, and pack thread-safe realization into a shared library, named ```libft_malloc_HOSTTYPE.so```, where ```HOSTTYPE``` is ```uname -m``` + ```_``` + ```uname -s```, also providing a symlink ```libft_malloc.so```.

## Installing

Clone this repository recursively:
```
git clone --recurse-submodules
```
, move into the folder
```
cd malloc && mkdir build && cd build
```
and
```
cmake .. && make
```
Run
```
make test
```
to execute unit tests.

## Using

This library can be added as a subdirectory to your cmake project.
It also can be build with sanitizers, just by adding one
of the following lines to your cmake configuration:
```
-DSANITIZE_ADDRESS=On
-DSANITIZE_MEMORY=On
-DSANITIZE_THREAD=On
-DSANITIZE_UNDEFINED=On
```

## License
![gplv3-88x31.png](https://www.gnu.org/graphics/gplv3-88x31.png)

This project is licensed under the GNU GPL License, Version 3 - see [LICENSE.md](LICENSE.md) for details.

## Contributing/Issue creating

Raise an issue in the issue tracker, or write me a letter.

## Contacts

* [Bitbucket](https://bitbucket.org/Mitriksicilian/)
* [E-mail](mailto:MitrikSicilian@icloud.com?subject=malloc from Bitbucket)
* MitrikSicilian@icloud.com

## Many thanks

* to [sanitizers for cmake](https://github.com/arsenm/sanitizers-cmake/)
* to [Google test](https://github.com/google/googletest)
* to UNIT Factory, for inspiration to do my best.
* to all UNIT Factory students, who shared their knowledge with me and tested this project.

## UNIT Factory
![UNIT Factory logo](https://unit.ua/static/img/logo.png)

[UNIT Factory](https://uk.wikipedia.org/wiki/UNIT_Factory) was an innovative programming school and a part of [School 42](https://en.wikipedia.org/wiki/42_(school)) in [the heart](https://en.wikipedia.org/wiki/Kyiv) of [Ukraine](https://en.wikipedia.org/wiki/Ukraine).
